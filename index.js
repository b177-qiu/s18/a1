let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
	friends: {
		hoenn: ["May, Max"], 
		kanto: ["Brock", "Misty"]},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}
console.log(trainer)

console.log("Result of dot notation:");
console.log(trainer.name);

console.log("Result of square bracket notation:");
console.log(trainer['pokemon'])

console.log("Result of talk method:");

trainer.talk()


// 8. Create a constructor for creating a pokemon with the following properties:
// - Name (Provided as an argument to the contructor)
// - Level (Provided as an argument to the contructor)
// - Health (Create an equation that uses the level property)
// - Attack (Create an equation that uses the level property)

function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		let newHealth = target.health-this.attack;
		console.log(target.name +"'s health is now reduced to " + newHealth);
		if (target.health-this.attack){
			target.health = newHealth
		}

		if (newHealth <=0){
			target.faint();
		}
	}
	this.faint = function(){
		console.log(this.name + ' fainted');
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log (pikachu)
console.log(geodude)
console.log(mewtwo)

geodude.tackle(pikachu)

console.log (pikachu)

mewtwo.tackle(geodude)

console.log(geodude)
